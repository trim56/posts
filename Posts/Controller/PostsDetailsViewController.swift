//
//  PostsDetailsViewController.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

class PostsDetailsViewController: UIViewController
{
    @IBOutlet weak var detailsView: UITextView!
    var detailsText: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        detailsView.text = detailsText
    }
}
