//
//  ViewController.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

class PostsViewController: UIViewController
{
    @IBOutlet weak var tableView: UITableView!
    
    private let dataSource = PostsModel()
    private var dataTable = [PostsModelData]()
    {
        didSet
        {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    private var preload: UIView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        preload = self.createPreload()
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView.tableFooterView = UIView()
        dataSource.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        dataSource.getData()
    }
    
    @IBAction func unwindController (sender: UIStoryboardSegue){}
}

extension PostsViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let mvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PostsDetails") as? PostsDetailsViewController
        {
            let text = dataTable[indexPath.row].body
            mvc.detailsText = text
            self.present(mvc, animated: true, completion: nil)
        }
    }
}

extension PostsViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as? PostsTableCell
        {
            cell.set(data: dataTable[indexPath.item])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataTable.count
    }
}

extension PostsViewController: PostsModelDelegate
{
    func updateData(data: [PostsModelData])
    {
        dataTable = data
        preload?.removeFromSuperview()
    }
    
    func updateFailData(error: Error)
    {
        self.present(Alert.dialogAlert(error: error), animated: true)
    }
}
