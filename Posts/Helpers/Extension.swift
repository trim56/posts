//
//  Extension.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

extension UIViewController
{
    func createPreload() -> UIView
    {
        let view = UIView(frame: self.view.frame)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        
        let preload = UIActivityIndicatorView(style: .large)
        preload.translatesAutoresizingMaskIntoConstraints = false
        preload.startAnimating()
        view.addSubview(preload)
        
        NSLayoutConstraint.activate([
            view.centerYAnchor.constraint(equalTo: preload.centerYAnchor),
            view.centerXAnchor.constraint(equalTo: preload.centerXAnchor),
            
            view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            view.topAnchor.constraint(equalTo: self.view.topAnchor),
            view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
        return view
    }
}

