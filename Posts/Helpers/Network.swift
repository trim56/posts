//
//  Network.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

class Network
{
    class func connect<T: Decodable>(
        url: String,
        method: String,
        model: T.Type,
        completion: @escaping (_ error: Error?, _ modelData: [T]) -> Void
    )
    {
        let baseURL: String = "https://jsonplaceholder.typicode.com"
        var errorReturn: Error!
        var dataReturn: [T] = []
        var request = URLRequest(url: URL(string: baseURL + url)!)
        request.httpMethod = method
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                let code = httpResponse.statusCode
                switch code {
                case 200...299:
                    do
                    {
                        dataReturn = try JSONDecoder().decode([T].self, from: data!)
                    } catch(let modelErr)
                    {
                        errorReturn = modelErr
                    }
                default:
                    errorReturn = error
                }
            }
            DispatchQueue.main.async {
                completion(errorReturn, dataReturn)
            }
        }).resume()
    }
}
