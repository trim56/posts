//
//  PostsTableCell.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

class PostsTableCell: UITableViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func set(data: PostsModelData)
    {
        self.textLabel?.numberOfLines = 0
        self.textLabel?.textColor = .black
        self.textLabel?.text = data.title
    }
    
}
