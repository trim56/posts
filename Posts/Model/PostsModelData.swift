//
//  PostsModelData.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

struct PostsModelData: Codable
{
    var title: String
    var body: String
}
