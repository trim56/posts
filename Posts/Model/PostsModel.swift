//
//  PostsModel.swift
//  Posts
//
//  Created by Павел Зорин on 16.05.2020.
//  Copyright © 2020 Zorin. All rights reserved.
//

import UIKit

protocol PostsModelDelegate: class
{
    func updateData(data: [PostsModelData])
    func updateFailData(error: Error)
}

class PostsModel: NSObject
{
    weak var delegate: PostsModelDelegate?
    
    func getData() {
        Network.connect(
            url: "/posts",
            method: "GET",
            model: PostsModelData.self
        ) { error, modelData in
            
            if let error = error
            {
                self.delegate?.updateFailData(error: error)
            } else
            {
                self.delegate?.updateData(data: modelData)
            }
        }
    }
}

