# Posts

## Краткое описание
Показ постов с https://jsonplaceholder.typicode.com
* Текущая версия приложения: 1.0
* Минимальная версия iOS: 13.0
* Лицензия: [MIT](https://opensource.org/licenses/MIT)

## Установка
1. Клонировать проект
2. Запустить Posts.xcodeproj в среде разработки Xcode
3. Собрать проект на симуляторе или физическом устройстве (iPhone)